
# TopThreads

TopThreads is a JConsole plugin to analyse CPU-usage per thread.

See <http://arnhem.luminis.eu/top-threads-plugin-for-jconsole/> and
<http://arnhem.luminis.eu/new_version_topthreads_jconsole_plugin/> for more info.

## Usage

To run TopThreads as JConsole plugin, pass the path to the jar file with the `-pluginpath` option:

    jconsole -pluginpath topthreads.jar

TopThreads can also be run as stand alone (Swing) application, and even as console application that runs in a terminal window! The latter is very usefull if you only have telnet or ssh access to a server you want to monitor and ssh tunneling and/or enabling remote jmx monitoring is not possible.

Running the console variant is as easy as typing

    java -jar topthreads.jar -t

or, equivalently but a little more verbose  

    java -jar topthreads.jar --textui

This will make TopThreads try to connect to local JVM's, just as JConsole does, and list the processes that can be monitored this way. If you already know the process id of the process you want to monitor, you can also pass it right away on the command line, as in

    java -jar topthreads.jar --textui 7837

The console variant needs two JDK libraries to run (tools.jar and jconsole.jar), which it tries to locate using the JAVA_HOME environment variable. If this variable is not set, it tries to guess it (by inspecting standard locations for the OS it's running on), but you can always override this by setting JAVA_HOME appropriately. If the JDK libraries are not present in your environment (which is possible when for example only the JRE is installed), you are bound to using remote jmx monitoring. 

To use remote jmx monitoring, make sure the application you want to monitor is started with the correct jmx options; see <http://docs.oracle.com/javase/8/docs/technotes/guides/management/agent.html> for details. For example, if you are ok with an insecure JMX connection, adding the following system properties will do:

    -Dcom.sun.management.jmxremote.authenticate=false
    -Dcom.sun.management.jmxremote.ssl=false
    -Dcom.sun.management.jmxremote.port=<port>

To connect TopThreads via remote jmx monitoring, you can just pass the connect address (in the form `<hostname>:<port>`) as a parameter, e.g.

    java -jar topthreads.jar --textui localhost:7007

where 7007 is the port that is configured for jmx remoting. The same will work with the Swing variant 
    
    java -jar topthreads.jar localhost:7007

but in this case, you are probably better off by using TopThreads as plugin in JConsole. 

## Building

The easiest way to build TopThreads from source, is by using gradle:

    gradle clean assemble
    
that generates the topthreads jar in build/libs. Note that the JAVA_HOME environment variable must be set for gradle to find the JDK libraries.

The Swing variant is most convenient for development, because it is easy to run or debug it in your favorite IDE.

## License

TopThreads is open source and licensed under LGPL (see the NOTICE.txt and LICENSE.txt files in the distribution). This means that you can use TopThreads for anything you like, and that you can embed it as a library in other applications, even commercial ones. If you do so, the author would appreciate if you include a reference to the original TopThreads application and website (one of the blogs mentioned above, or this readme). As of the LGPL license, all modifications and additions to the source code must be published as (L)GPL as well.

## Feedback

Any feedback is welcome. If you have questions or comments, post to one of the blogs mentioned above. If you discover a bug, please file an issue at <https://bitbucket.org/pjtr/topthreads/issues>.

